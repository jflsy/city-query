# 城市搜索插件
```
git clone https://gitee.com/jflsy/city-query.git
```
![](https://oscimg.oschina.net/oscnet/b9acb933e6b61934c86a94826d0eabaab2c.png)

&emsp;&emsp;引用插件时只需要src文件下的内容就可以了。
```
<script type='text/javascript' src='./src/jquery.js'></script>
<script type='text/javascript' src='./src/querycity.js'></script>
<link href='./src/cityquery.css' rel="stylesheet" type="text/css" />
```
&emsp;&emsp;在HTML中加上`<input id='fromcity' type='text' value='' />`input的输入框，给input输入框加上方法就可以。
```
$('#fromcity').querycity({'data':citysFlight,'tabs':labelFromcity,'hotList':hotList});
```
&emsp;&emsp;querycity的参数
- data（数据数组）
	```
	var citysFlight=new Array();
	citysFlight[0]=new Array('pek','北京','beijing','bj');
	citysFlight[1]=new Array('can','广州','guangzhou','gz');
	citysFlight[2]=new Array('szx','深圳','shenzhen','sz');
	citysFlight[3]=new Array('csx','长沙','changsha','cs');
	```
	&emsp;&emsp;本数据中的搜索数组是4个（都可以搜索），如果你的简写自有2个或3个，需要修改querycity.js文件（199行）
	```
	...
	            for(var item in options.data){			
                var _data = options.data[item];		
                if(typeof (_data) != 'undefined'){
                    if(_data[2].indexOf(value) >= 0 || _data[3].indexOf(value) >= 0 || _data[1].indexOf(value) >=0 || _data[0].indexOf(value) >=0 ){					                   
                        isHave = true;
                        _tmp.push(_data);
                    }
                }
             }
	...
	```
- tabs（tab数组）

	```
	var labelFromcity = new Array();
	labelFromcity ['热门城市'] = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21	,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40);
	labelFromcity ['A-F'] = new Array(0,3,4,5,6,28,29);
	labelFromcity ['G-J'] = new Array(1,7,8,9,30,31,32,33,37,40);
	labelFromcity ['K-N'] = new Array(10,11,12,34,35,38);
	labelFromcity ['P-W'] = new Array(13,14,15,16,17,18,22,24,25,36);
	labelFromcity ['X-Z'] = new Array(2,19,20,21,26,27,39);
	labelFromcity ['国际城市'] = new Array(41,42,43,44,45,46,47,48,49);
	```
	&emsp;&emsp;key值为显示项，value为data的数组索引列表。显示的效果：
	
	![](https://oscimg.oschina.net/oscnet/ad14ff513e658f873f4630022b042f709c7.png)
- hotList（热门城市列表索引）

	```
	var hotList = new Array(14,15,16,17,18,19);
	```
	![](https://oscimg.oschina.net/oscnet/38c8e41cd79864c607a3a1fc36cf67f1761.png)

&emsp;&emsp;很简单的插件，有需要就拿去吧！